﻿
using System.Collections.Generic;
using System.Windows;

namespace Crypto.Algorithms
{
	public class ViginerAlgoritm : IAlgorithm
	{

		public char[] Crypt(char[] inputText, char[] key, char[] alphabet)
		{
			var outText = new char[inputText.Length];
			var fullKey = GetFullKey(inputText, key);
	


			for(int i=0; i < inputText.Length; i++)
			{
				var nowIndexKey = GetIndexOf(alphabet, fullKey[i]);
				var shiftedSeq = GetShiftedSeq(alphabet, nowIndexKey);
				var nowIndexInput = GetIndexOf(alphabet, inputText[i]);
				outText[i] = shiftedSeq[nowIndexInput];
			}
			return outText;
		}

		public char[] Decipher(char[] inputText, char[] key, char[] alphabet)
		{
			var outText = new char[inputText.Length];
			var fullKey = GetFullKey(inputText, key);
			


			for (int i = 0; i < inputText.Length; i++)
			{
				var nowIndexKey = GetIndexOf(alphabet, fullKey[i]);
				var shiftedSeq = GetShiftedSeq(alphabet, nowIndexKey);
				var nowIndexInput = GetIndexOf(shiftedSeq, inputText[i]);
				outText[i] = alphabet[nowIndexInput];
			}
			return outText;
		}

		private char[] GetFullKey(char[] inputText, char[] key)
		{
			var fullKey = key;
			if (inputText.Length != key.Length)
				fullKey = GetFullKey(key, inputText.Length);
			return fullKey;
		}

		private int GetIndexOf(char[] seq, char ch)
		{
			var nowIndex = -1;
			for(int i=0; i < seq.Length; i++)
			{
				if (ch == seq[i])
				{
					nowIndex = i;
					break;
				}
			}
			return nowIndex;
		}

		private char[] GetFullKey(char[] key, int lenghtInputText)
		{
			var fullKey = new char[lenghtInputText];
			for(int i = 0; i < lenghtInputText; i++)
			{
				if (i < key.Length)
					fullKey[i] = key[i];
				else
					fullKey[i] = fullKey[i - key.Length];

			}
			return fullKey;
		}

		

		private char[] GetShiftedSeq(char[] inputSeq, int shiftValue)
		{
			var outSeq = new char[inputSeq.Length];

			var index = 0;

			while(shiftValue > index)
			{
				var nowIndex = index + shiftValue;
				if (nowIndex >= inputSeq.Length)
				{
					var nowCoef = nowIndex / inputSeq.Length;
					if (nowCoef < 1)
						nowCoef = 1;
					nowIndex = nowIndex / nowCoef - inputSeq.Length;
				}
				outSeq[index] = inputSeq[nowIndex];
				outSeq[inputSeq.Length  - shiftValue + index ] = inputSeq[index];
				index++;
			}

			for(int i = shiftValue; i < inputSeq.Length - shiftValue; i++)
			{
				outSeq[i] = inputSeq[i + shiftValue];
			}

			return outSeq;
			
		}

	}
}
