﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Crypto.Algorithms
{
    public class SingleGamming : IAlgorithm
    {

		public char[] Crypt(char[] inputArray, char[] key, char[] allSymbols)
		{
			var sum = new char[inputArray.Length];
			for(int i=0;i< inputArray.Length; i++)
            {
				sum[i] = Xor(inputArray[i], key[i]);
            }

			return sum;

		}

		private char Xor(char first, char second)
        {
			return char.Parse((int.Parse(first.ToString()) ^ int.Parse(second.ToString())).ToString());
        }

		public char[] Decipher(char[] inputArray, char[] key, char[] allSymbols)
		{
			return Crypt(inputArray, key, allSymbols);
		}

		private int ParseKey(char[] key)
		{
			string nowKey = string.Empty;
			foreach (var ch in key)
				nowKey += ch;
			return int.Parse(nowKey);
		}

		

	}
}
