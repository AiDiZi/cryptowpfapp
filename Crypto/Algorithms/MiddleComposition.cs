﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crypto.Algorithms
{
    public class MiddleComposition : IAlgorithm
    {

        public char[] Crypt(char[] inputArray, char[] number, char[] allSymbols)
        {
            var numbersStr = new string(inputArray).Split(' ');
            var iteration = 0;
            for (int i = 0; i < number.Length; i++)
                iteration += int.Parse(number[i].ToString()) * (int)Math.Pow(10, number.Length - 1);

            var firstNum = int.Parse(numbersStr[0]);
            var secondNum = int.Parse(numbersStr[1]);
            var randomSeq = 0;
            for (int i = 0; i < iteration; i++)
            {
                var composition = firstNum * secondNum;
                var compostionStr = composition.ToString();
                var deleteCount = 2;
                if (compostionStr.Length == 7)
                    deleteCount = 1;
                if (compostionStr.Length > 4)
                {
                    compostionStr = compostionStr.Remove(0, deleteCount);
                    compostionStr = compostionStr.Remove(compostionStr.Length - 2, 2);
                }
                randomSeq = int.Parse(compostionStr);
                firstNum = secondNum;
                secondNum = randomSeq;
            }

            return randomSeq.ToString().ToCharArray();
        }

        public char[] Decipher(char[] inputStr, char[] key, char[] alphabet)
        {
            return Crypt(inputStr, key, alphabet);
        }

    }
}
