﻿using Crypto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crypto.Algorithms
{
    public class LinearCongruentCipher : IAlgorithm
    {
        public char[] Crypt(char[] inputArray, char[] block, char[] allSymbols)
        {
            double startValue = double.Parse(new string(inputArray));
            var moduleFactorIncrement = Parse(block);

            return ((moduleFactorIncrement.Factor * startValue + moduleFactorIncrement.Increment) % moduleFactorIncrement.Module).ToString().ToCharArray();
            
        }

        public char[] Decipher(char[] inputStr, char[] key, char[] alphabet)
        {
            return Crypt(inputStr, key, alphabet);
        }

        private ModuleFactorIncrement Parse(char[] block) 
        {
            var linesStr = new string(block).Split('\n');
            return new ModuleFactorIncrement(double.Parse(linesStr[0]), double.Parse(linesStr[1]), double.Parse(linesStr[2]));
        }

    }
}
