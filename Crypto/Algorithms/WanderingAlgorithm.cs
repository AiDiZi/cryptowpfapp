﻿using Crypto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Crypto.Algorithms
{
    public class WanderingAlgorithm : IAlgorithm
    {
        public char[] Crypt(char[] inputStr, char[] key, char[] alphabet)
        {
            int nowKey = ParseKey(key);
            var matrix = GetMatrixForCrypt(inputStr, nowKey);

            var cryptStr = new List<char>();

            for (int i = 0; i < matrix.GetLength(1); i++)
            {
                for (int j = 0; j < matrix.GetLength(0); j++)
                {
                    cryptStr.Add(matrix[j, i]);
                }
            }
            return cryptStr.ToArray();
        }

        public char[] Decipher(char[] inputStr, char[] key, char[] alphabet)
        {
            int nowKey = ParseKey(key);
            var matrix = GetMatrixForDecipher(inputStr, nowKey);

            var cryptStr = new char[inputStr.Length];

            var nowIndex = 0;
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (matrix[i, j] != '*')
                    {
                        cryptStr[nowIndex] = matrix[i, j];
                        nowIndex++;
                    }
                }
            }
            return cryptStr;
        }

        private int ParseKey(char[] key)
        {
            string nowKey = string.Empty;
            foreach (var ch in key)
                nowKey += ch;
            return int.Parse(nowKey);
        }

        private char[,] GetMatrixForCrypt(char[] str, int rowsCount)
        {
            var Column = str.Length / rowsCount;
            if (Column * rowsCount < str.Length)
                Column++;
            char[,] matrix = new char[rowsCount, Column];

            var nowRow = 0;
            var nowColumn = 0;
            for (int i = 0; i < str.Length; i++)
            {
                matrix[nowRow, nowColumn] = str[i];
                nowColumn++;
                if ((i + 1) % Column == 0)
                {
                    nowRow++;
                    nowColumn = 0;
                }
            }
            return FillEmptyMatrix(matrix);
        }


        private char[,] GetMatrixForDecipher(char[] str, int rowsCount)
        {
            var Column = str.Length / rowsCount;
            if (Column * rowsCount < str.Length)
                Column++;
            char[,] matrix = new char[rowsCount, Column];

            var nowRow = 0;
            var nowColumn = 0;
            for (int i = 0; i < str.Length; i++)
            {
                matrix[nowRow, nowColumn] = str[i];
                nowRow++;
                if ((i + 1) % rowsCount == 0)
                {
                    nowColumn++;
                    nowRow = 0;
                }
            }

            return FillEmptyMatrix(matrix);
        }

        private char[,] FillEmptyMatrix(char[,] matrix)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (matrix[i, j] == '\0')
                        matrix[i, j] = '*';
                }
            }

            return matrix;
        }

    }
}
