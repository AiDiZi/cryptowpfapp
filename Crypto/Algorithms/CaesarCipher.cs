﻿using Crypto.Algorithms;
using System;
using System.Collections.Generic;
using System.Windows;

namespace Crypto.Algorithms
{
	public class CaesarCipher : IAlgorithm
	{
		public char[] Crypt(char[] inputText, char[] key, List<Point> points)
		{
			return inputText;
		}
		public char[] Crypt(char[] inputArray, char[] offset, char[] allSymbols )
		{
			return Realization(inputArray, offset, allSymbols, true);
			
		}

		public char[] Decipher(char[] inputArray, char[] offset, char[] allSymbols)
		{
			return Realization(inputArray, offset, allSymbols, false);
		}

		private int ParseKey(char[] key)
		{
			string nowKey = string.Empty;
			foreach (var ch in key)
				nowKey += ch;
			return int.Parse(nowKey);
		}

		private char[] Realization(char[] inputArray, char[] offset, char[] allSymbols, bool isCrypt)
		{
			var key = ParseKey(offset);
			var arg = 1;

			if (!isCrypt)
				arg = -1;

			char[] outSymbols = new char[inputArray.Length];

			CircleArray circleArray = new CircleArray(allSymbols);

			for (int i = 0; i < inputArray.Length; i++)
			{
				var indexRegister = circleArray.GetIndexBySymbols(inputArray[i]);
				char nowSymbol = inputArray[i];
				if (indexRegister != -1)
					nowSymbol = circleArray.GetSymbolsByIndex(indexRegister + key * arg);
				outSymbols[i] = nowSymbol;
			}

			return outSymbols;
		}

		
	}
}
