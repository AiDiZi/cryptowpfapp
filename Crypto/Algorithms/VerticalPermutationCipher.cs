﻿using Crypto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Crypto.Algorithms
{
    public class VerticalPermutationCipher : IAlgorithm
    {

        public char[] Crypt(char[] inputStr, char[] key, char[] alphabet)
        {
            var matrix = GetMatrixForCrypt(inputStr, key.Length);

            var sortedPairs = GetOrderOfFollowing(alphabet, key);

            var cryptStr = string.Empty;

            foreach(var pair in sortedPairs)
            {
                for(int i=0; i < matrix.GetLength(0); i++)
                {
                    cryptStr += matrix[i, pair.OldIndex];
                }
            }

            return cryptStr.ToCharArray();
        }

        private List<OldNewIndex> GetOrderOfFollowing(char[] alphabet, char[] key)
        {
            CircleArray circleArray = new CircleArray(alphabet);
            var pairs = new List<OldNewIndex>();
            for (int i = 0; i < key.Length; i++)
            {
                var numberCh = circleArray.GetIndexBySymbols(key[i]);
                pairs.Add(new OldNewIndex(i, numberCh));
            }

            return pairs.OrderBy(item => item.NewIndex).ToList();
        }

        public char[] Decipher(char[] inputStr, char[] key, char[] alphabet)
        {
            var sortedPairs = GetOrderOfFollowing(alphabet, key);
            var matrix = GetMatrixForDecipher(inputStr, key.Length, sortedPairs);
            var outStr = string.Empty;
            for(int i=0;i < matrix.GetLength(0); i++)
            {
                for(int j=0; j< matrix.GetLength(1); j++)
                {
                    if(matrix[i,j] != '*')
                        outStr += matrix[i, j];
                }
            }
            return outStr.ToCharArray();
        }

        private char[,] GetMatrixForDecipher(char[] str, int Column, List<OldNewIndex> sortedKeyIndex)
        {
            var rowsCount = str.Length / Column;
            if (Column * rowsCount < str.Length)
                rowsCount++;
            char[,] matrix = new char[rowsCount, Column];

            var nowIndex = 0;
            foreach(var pair in sortedKeyIndex)
            {
                for(int i = 0; i< matrix.GetLength(0); i++)
                {
                    if (nowIndex < str.Length)
                    {
                        matrix[i, pair.OldIndex] = str[nowIndex];
                        nowIndex++;
                    }
                }
            }

            return FillEmptyMatrix(matrix);
        }
        private char[,] GetMatrixForCrypt(char[] str, int Column)
        {
            var rowsCount = str.Length / Column;
            if (Column * rowsCount < str.Length)
                rowsCount++;
            char[,] matrix = new char[rowsCount, Column];
            var nowRow = 0;
            var nowColumn = 0;
            for (int i = 0; i < str.Length; i++)
            {
                matrix[nowRow, nowColumn] = str[i];
                nowColumn++;
                if ((i + 1) % Column == 0)
                {
                    nowRow++;
                    nowColumn = 0;
                }
            }
            return FillEmptyMatrix(matrix);
        }

        private char[,] FillEmptyMatrix(char[,] matrix)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (matrix[i, j] == '\0')
                        matrix[i, j] = '*';
                }
            }

            return matrix;
        }


    }
}
