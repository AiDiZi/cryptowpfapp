﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Crypto.Algorithms
{
    public class SwivelGrille : IAlgorithm
    {

        public char[] Crypt(char[] inputStr, char[] key, char[] alphabet)
        {
            var grille = GetPoints(key);
            var sortedPoints = SortPoints(grille);
            int rowsCount = ParseKey(key);
            var Column = inputStr.Length / rowsCount;
            if (Column * rowsCount < inputStr.Length)
                Column++;
            char[,] matrix = new char[rowsCount, Column];

            var nowIndex = 0;
            for (int i = 0; i < 4; i++)
            {
                matrix = FillMatrix(matrix, sortedPoints, nowIndex, inputStr);
                nowIndex += sortedPoints.Count;
                if (i == 1)
                    sortedPoints = VertexGrilleByVercticle(sortedPoints, matrix.GetLength(0));
                else
                    sortedPoints = VertexGrilleByHorizontal(sortedPoints, matrix.GetLength(1));
            }

            matrix = FillEmptyMatrix(matrix);

            var cryptStr = new List<char>();

            for (int i = 0; i < matrix.GetLength(1); i++)
            {
                for (int j = 0; j < matrix.GetLength(0); j++)
                {
                    cryptStr.Add(matrix[j, i]);
                }
            }
            return cryptStr.ToArray();
        }

        private int ParseKey(char[] inputStr)
        {
            var input = string.Empty;
            for (int i = 0; i < inputStr.Length; i++)
                input += inputStr[i];

            var inputArr = input.Split('\n');

            return int.Parse(inputArr[0]);
        }

        private List<Point> GetPoints(char[] inputStr)
        {
            var points = new List<Point>();
            var input = string.Empty;
            for (int i = 0; i < inputStr.Length; i++)
                input += inputStr[i];

            var inputArr = input.Split('\n');

            for(int i = 1; i< inputArr.Length; i++)
            {
                var nowPointStr = inputArr[i].Trim();
                var firstCoordinat = double.Parse(nowPointStr.Split(' ')[0]) - 1 ;
                var secondCoordinat = double.Parse(nowPointStr.Split(' ')[1]) - 1;
                points.Add(new Point(firstCoordinat, secondCoordinat));
            }

            return points;
        }

      
        private char[,] FillMatrix(char[,] matrix, List<Point> nowPoints, int nowIndex, char[] inputStr)
        {
            foreach (var point in nowPoints)
            {
                if(nowIndex < inputStr.Length)
                    matrix[(int)point.X, (int)point.Y] = inputStr[nowIndex];
                nowIndex++;
            }
            return matrix;
        }

        private List<Point> SortPoints(List<Point> points)
        {
            var sortedPoint = from point in points
                              orderby point.X, point.Y
                              select point;
            return sortedPoint.ToList();
        }

        private List<Point> VertexGrilleByVercticle(List<Point> points,int matrixColumn)
        {
            var newPoints = new List<Point>();
            foreach(var point in points)
            {
                int nowDiff = 0;
                if (point.X < matrixColumn / 2) 
                {
                    nowDiff = (int)point.X;
                    newPoints.Add(new Point(matrixColumn - nowDiff - 1 , point.Y));
                }
                else 
                {
                    nowDiff = matrixColumn - (int)point.X;
                    newPoints.Add(new Point(nowDiff - 1, point.Y));
                }

            }
            return SortPoints(newPoints);
        }

        private List<Point> VertexGrilleByHorizontal(List<Point> points, int matrixRow)
        {
            var newPoints = new List<Point>();
            foreach (var point in points)
            {
                int nowDiff = 0;
                if (point.Y < matrixRow / 2)
                {
                    nowDiff = (int)point.Y ;
                    newPoints.Add(new Point(point.X, matrixRow - nowDiff - 1));
                }
                else
                {
                    nowDiff = matrixRow - (int)point.Y;
                    newPoints.Add(new Point(point.X,nowDiff - 1));
                }

            }
            return SortPoints(newPoints);
        }

        public char[] Decipher(char[] inputStr, char[] key, char[] alphabet)
        {
            var grille = GetPoints(key);
            var sortedPoints = SortPoints(grille);
            int nowKey = ParseKey(key);
            var row = inputStr.Length / nowKey;
            if (row * nowKey < inputStr.Length)
                row++;
            var matrix = GetMatrixForDecipher(inputStr, row);

            var cryptStr = new List<char>();

            for (int i = 0; i < 4; i++)
            {
                foreach(var point in sortedPoints)
                {
                    if(matrix[(int)point.X, (int)point.Y] != '*')
                        cryptStr.Add(matrix[(int)point.X, (int)point.Y]);
                }
                if (i == 1)
                    sortedPoints = VertexGrilleByVercticle(sortedPoints, matrix.GetLength(0));
                else
                    sortedPoints = VertexGrilleByHorizontal(sortedPoints, matrix.GetLength(1));
            }

            return cryptStr.ToArray();
        }



        private char[,] GetMatrixForDecipher(char[] str, int rowsCount)
        {
            var Column = str.Length / rowsCount;
            if (Column * rowsCount < str.Length)
                Column++;
            char[,] matrix = new char[rowsCount, Column];

            var nowRow = 0;
            var nowColumn = 0;
            for (int i = 0; i < str.Length; i++)
            {
                matrix[nowRow, nowColumn] = str[i];
                nowRow++;
                if ((i + 1) % rowsCount == 0)
                {
                    nowColumn++;
                    nowRow = 0;
                }
            }

            return FillEmptyMatrix(matrix);
        }

        private char[,] FillEmptyMatrix(char[,] matrix)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (matrix[i, j] == '\0')
                        matrix[i, j] = '*';
                }
            }

            return matrix;
        }
    }
}
