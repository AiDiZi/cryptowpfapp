﻿
using System.Collections.Generic;
using System.Windows;

namespace Crypto.Algorithms
{
	public interface IAlgorithm
	{
		char[] Crypt(char[] inputText, char[] key, char[] alphabet);

		char[] Decipher(char[] inputText, char[] key, char[] alphabet);

	}
}
