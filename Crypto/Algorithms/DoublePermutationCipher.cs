﻿using Crypto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Crypto.Algorithms
{
    public class DoublePermutationCipher : IAlgorithm
    {
        public char[] Crypt(char[] inputText, char[] key, List<Point> points)
        {
            return inputText;
        }
        public char[] Crypt(char[] inputStr, char[] key, char[] alphabet)
        {
            var keys = GetKeys(key);

            var matrix = GetMatrix(inputStr, keys[0].Length, keys[1].Length);

            var sortedPairsColumn = GetOrderOfFollowing(alphabet, keys[0].ToCharArray());

            var sortedPairsRow = GetOrderOfFollowing(alphabet, keys[1].ToCharArray());

            var cryptStr = string.Empty;

            foreach (var pairRow in sortedPairsRow)
            {
                foreach (var pairColumn in sortedPairsColumn)
                {
                    cryptStr += matrix[pairRow.OldIndex, pairColumn.OldIndex];
                }
            }

            return cryptStr.ToCharArray();
        }

        private string[] GetKeys(char[] key)
        {
            var keyStr = string.Empty;
            for (int i = 0; i < key.Length; i++)
                keyStr += key[i];
            return keyStr.Split(' ');
        }

        private List<OldNewIndex> GetOrderOfFollowing(char[] alphabet, char[] key)
        {
            CircleArray circleArray = new CircleArray(alphabet);
            var pairs = new List<OldNewIndex>();
            for (int i = 0; i < key.Length; i++)
            {
                var numberCh = circleArray.GetIndexBySymbols(key[i]);
                pairs.Add(new OldNewIndex(i, numberCh));
            }

                return pairs.OrderBy(item => item.NewIndex).ToList();
        }

        public char[] Decipher(char[] inputStr, char[] key, char[] alphabet)
        {
            var keys = GetKeys(key);


            var sortedPairsColumn = GetOrderOfFollowing(alphabet, keys[0].ToCharArray());

            var sortedPairsRow = GetOrderOfFollowing(alphabet, keys[1].ToCharArray());

            var matrix = GetMatrixForDecipher(inputStr, sortedPairsRow, sortedPairsColumn);


            var cryptStr = string.Empty;

            for(int i = 0; i < matrix.GetLength(0); i++)
            {
                for(int j=0; j< matrix.GetLength(1); j++)
                {
                    if (matrix[i, j] != '*')
                        cryptStr += matrix[i, j];
                }
            }

            return cryptStr.ToCharArray();

        }

        private char[,] GetMatrixForDecipher(char[] str, List<OldNewIndex> sortedRowIndex, List<OldNewIndex> sortedColumnIndex)
        {
            
            char[,] matrix = new char[sortedRowIndex.Count, sortedColumnIndex.Count];

            var nowIndex = 0;
            foreach (var pair in sortedRowIndex)
            {
                foreach(var insidePair in sortedColumnIndex)
                {
                    matrix[pair.OldIndex, insidePair.OldIndex] = str[nowIndex];
                    nowIndex++;
                }
            }

            return FillEmptyMatrix(matrix);
        }


        private char[,] GetMatrix(char[] str, int Column, int rowsCount)
        {
            char[,] matrix = new char[rowsCount, Column];
            var nowIndex = 0;
            for(int i=0;i< rowsCount; i++)
            {
                for(int j = 0; j < Column; j++)
                {
                    if (nowIndex < str.Length)
                        matrix[i, j] = str[nowIndex];
                    nowIndex++;
                }
            }

            return FillEmptyMatrix(matrix);
        }

        private char[,] FillEmptyMatrix(char[,] matrix)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (matrix[i, j] == '\0')
                        matrix[i, j] = '*';
                }
            }

            return matrix;
        }


    }
}
