﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crypto.Algorithms
{
    public class MiddleSquares : IAlgorithm
    {

        public char[] Crypt(char[] inputArray, char[] number, char[] allSymbols)
        {
            double startDouble = 0;
            for(int i=2; i < inputArray.Length; i++)
            {
                var nowPow = Math.Pow(10, 1 - i);
                var nowCh = double.Parse(inputArray[i].ToString());
                startDouble += nowCh * nowPow;
            }

            int iteration = int.Parse(new string(number));
            

            for(int i=0; i < iteration; i++)
            {
                startDouble = Math.Round(startDouble * startDouble, 8);
                var startDoubleDec = (int)(startDouble * Math.Pow(10, 6));
                var str = startDoubleDec.ToString();
                var newDec = string.Empty;
                for(int j= 2; j < str.Length ; j++) 
                {
                    newDec += str[j];
                }

                newDec = newDec.Insert(0, ",").Insert(0, "0");

                startDouble = Convert.ToDouble(newDec);

            }

            var outStr = string.Empty;
            if (startDouble == 0)
                outStr = "0";
            else
                outStr = startDouble.ToString().Substring(2);

            return outStr.ToCharArray();
        }

        public char[] Decipher(char[] inputStr, char[] key, char[] alphabet)
        {
            return Crypt(inputStr, key, alphabet);
        }

    }
}
