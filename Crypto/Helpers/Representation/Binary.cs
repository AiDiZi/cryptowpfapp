﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Crypto.Helpers.Representation
{
    public class Binary : Representation
    {
        public Binary()
        {
            this.UnicodeCharLenght = 8;
        }

        public override string GetRepresentation(string inputText)
        {
            var inputByteArr = GetBytes(inputText);

            var binary = new List<char>();

            foreach (var byt in inputByteArr)
            {
                var nowArr = GetBinaryArr(byt);
                for (int i = 0; i < nowArr.Length; i++)
                    binary.Add(nowArr[i]);
            }

            var outTxt = string.Empty;
            foreach (var ch in binary)
                outTxt += ch;

            return outTxt;
        }

        public override string GetRepresentation(byte[] inputByteArr)
        {
            var binary = new List<char>();

            foreach (var byt in inputByteArr)
            {
                var nowArr = GetBinaryArr(byt);
                for (int i = 0; i < nowArr.Length; i++)
                    binary.Add(nowArr[i]);
            }

            var outTxt = string.Empty;
            foreach (var ch in binary)
                outTxt += ch;

            return outTxt;
        }


        private char[] GetBinaryArr(byte number)
        {
            var arr = new List<char>();
            var nowNum = number;
            while (nowNum != 0)
            {
                var nowResidue = nowNum % 2;
                if (nowResidue == 0)
                    arr.Add('0');
                else
                    arr.Add('1');
                nowNum /= 2;
            }
            arr.Reverse();
            while (arr.Count < 8)
            {
                arr.Insert(0, '0');
            }
            return arr.ToArray();

        }

        public override byte GetDecimalBytesArr(string stream)
        {
            double nowByte = 0; 
            for(int i=0; i< stream.Length; i++)
            {
                nowByte += int.Parse(stream[i].ToString()) * Math.Pow(2, stream.Length - 1 - i);
            }
            return Convert.ToByte(nowByte);
        }

    }
}
