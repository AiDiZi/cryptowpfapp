﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crypto.Helpers.Representation
{
    public class Hex : Representation
    {

        public Hex()
        {
            this.UnicodeCharLenght = 2;
        }

        public override string GetRepresentation(string inputText)
        {
            var inputByteArr = GetBytes(inputText);
            return BitConverter.ToString(inputByteArr).Replace("-", "");
        }

        public override string GetRepresentation(byte[] bytes)
        {
            return BitConverter.ToString(bytes).Replace("-", "");
        }

        public override byte GetDecimalBytesArr(string stream)
        {
            double nowByte = 0;
            for (int i = 0; i < stream.Length; i++)
            {
                nowByte += GetOrigin(stream[i]) * Math.Pow(16, stream.Length - 1 - i);
            }
            return Convert.ToByte(nowByte);
        }

        private int GetOrigin(char ch)
        {
            var nowNum = 0;
            switch (ch)
            {
                case 'A':
                    nowNum = 10;
                    break;
                case 'B':
                    nowNum = 11;
                    break;
                case 'C':
                    nowNum = 12;
                    break;
                case 'D':
                    nowNum = 13;
                    break;
                case 'E':
                    nowNum = 14;
                    break;
                case 'F':
                    nowNum = 15;
                    break;
                default:
                    nowNum = int.Parse(ch.ToString());
                    break;
            }
            return nowNum;
        }


    }
}
