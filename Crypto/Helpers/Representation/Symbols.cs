﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crypto.Helpers.Representation
{
    public class Symbols : Representation
    {
        public Symbols()
        {
            this.UnicodeCharLenght = 8;
        }

        public override string GetRepresentation(string inputText)
        {
            return inputText;
        }

        public override string GetRepresentation(byte[] bytes)
        {
            return Encoding.Unicode.GetString(bytes);
        }



        public override byte[] GetArrBytes(string stream)
        {
            return GetBytes(stream);
        }

    }
}
