﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crypto.Helpers.Representation
{
    public abstract class Representation
    {
        public abstract string GetRepresentation(string inputText);

        public abstract string GetRepresentation(byte[] bytes);

        protected int UnicodeCharLenght = 4;

 
        public virtual byte[] GetArrBytes(string inputText)
        {
            var tetraList = new List<string>();

            var nowTetra = string.Empty;

            for (int i = 0; i < inputText.Length; i++)
            {
                nowTetra += inputText[i];
                if ((i + 1) % UnicodeCharLenght == 0)
                {
                    tetraList.Add(nowTetra);
                    nowTetra = string.Empty;
                }

            }

            var nowArrBytes = new List<byte>();

            foreach (var tetra in tetraList)
            {
                var nowByte = GetDecimalBytesArr(tetra);
                nowArrBytes.Add(nowByte);
            }

            return nowArrBytes.ToArray();

        }

        public virtual byte GetDecimalBytesArr(string stream)
        {
            return 255;
        }


        protected byte[] GetBytes(string input)
        {
            Encoding encodingUnicode = Encoding.Unicode;
            return encodingUnicode.GetBytes(input);
        }
        
    }

    }
