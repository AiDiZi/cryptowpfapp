﻿using Crypto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crypto.Helpers
{
	public class OriginStaticstics
	{

		public static List<CharCountPair> GetCharCountPairsRussian()
		{
			var pairs = new List<CharCountPair>();
			pairs.Add(new CharCountPair('о', 0, 0.10983));
			pairs.Add(new CharCountPair('е', 0, 0.08483));
			pairs.Add(new CharCountPair('а', 0, 0.07998));
			pairs.Add(new CharCountPair('и', 0, 0.07367));
			pairs.Add(new CharCountPair('н', 0, 0.067));
			pairs.Add(new CharCountPair('т', 0, 0.006138));

			return pairs;
		}

		public static List<CharCountPair> GetCharCountPairsEnglish()
		{
			var pairs = new List<CharCountPair>();
			pairs.Add(new CharCountPair('e', 0, 0.127));
			pairs.Add(new CharCountPair('t', 0, 0.091));
			pairs.Add(new CharCountPair('a', 0, 0.082));
			pairs.Add(new CharCountPair('o', 0, 0.075));
			pairs.Add(new CharCountPair('i', 0, 0.07));
			pairs.Add(new CharCountPair('n', 0, 0.068));

			return pairs;
		}

	}
}
