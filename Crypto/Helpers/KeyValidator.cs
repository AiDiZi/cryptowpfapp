﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;

namespace Crypto.Helpers
{
	public class KeyValidator
	{

		public static bool IsValidKeyNumbersChars(string key, string inputStr)
		{
			string pattern = @"^[a-zA-Z0-9-а-яА-Я]+$";
			Regex reg = new Regex(pattern);
			return reg.IsMatch(key);
		}

		public static bool IsValidKeyNumber(string key, string inputStr)
		{
			int res;
			if (Int32.TryParse(key, out res))
				return true;
			else
				return false;
		}

		public static bool IsValidTwoKeys(string key, string inputStr)
        {
			var strArr = key.Split(' ');
			if (strArr.Length == 2)
			{
				string pattern = @"^[a-zA-Z0-9-а-яА-Я]+$";
				Regex reg = new Regex(pattern);
				return (reg.IsMatch(strArr[0]) && reg.IsMatch(strArr[1]));
			}
			else
				return false;
				
        }

		public static bool IsValidGrilleKey(string inputStr, string str)
        {
			var result = true;
            var points = new List<Point>();
            var input = string.Empty;
            for (int i = 0; i < inputStr.Length; i++)
                input += inputStr[i];

            var inputArr = input.Split('\n');

			try
			{
				if (!IsValidKeyNumber(inputArr[0], ""))
					result = false;
				for (int i = 1; i < inputArr.Length; i++)
				{
					var nowPointStr = inputArr[i].Trim();
					var firstCoordinat = double.Parse(nowPointStr.Split(' ')[0]) ;
					var secondCoordinat = double.Parse(nowPointStr.Split(' ')[1]) ;
					points.Add(new Point(firstCoordinat, secondCoordinat));
				}

				foreach(var point in points)
                {
					if(point.Y > int.Parse(inputArr[0]) || point.Y <= 0 || point.X <= 0)
                    {
						result = false;
                    }
                }
			}
            catch
            {
				result = false;
            }

			return result;
           
        }

		public static bool IsKeyEqStr(string key, string input)
        {
			if (key.Length >= input.Length)
				return true;
			else
				return false;
        }

		public static bool IsValidKeyScrem(string key, string input)
        {
			var rows = key.Split('\n');
			if(rows.Length == 2)
            {
				var polinom = rows[0];
				var register = rows[1];
				if (register.ToList().All(item => item == '0' || item == '1') && !string.IsNullOrWhiteSpace(register))
				{
					return true;
				}
				else
					return false;
            }
            else
            {
				return false;
            }
        }

		public static bool IsValidModuleFactorInc(string key, string input)
        {
			var rows = key.Split('\n');

			if(rows.Length == 3)
            {
				double module = 0;
				double factor = 0;
				double inc = 0;
				double startValue = 0;
				if (double.TryParse(rows[0], out module) && double.TryParse(rows[1], out factor) && double.TryParse(rows[2], out inc) && double.TryParse(input, out startValue))
				{
					if (module > 0 && factor >= 0 && inc >= 0 && startValue >= 0)
					{
						if (factor < module && inc < module && startValue < module)
							return true;
						else
							return false;
					}
					else
						return false;
				}
				else
					return false;
            }
            else
            {
				return false;
            }
        }




	}
}
