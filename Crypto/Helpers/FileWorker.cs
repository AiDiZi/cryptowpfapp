﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.Windows.Forms;

namespace Crypto
{
	public class FileWorker
	{

		public string ReadFromFile(string path)
		{
			var str = "";
			using (StreamReader reader = new StreamReader(path))
			{
				str = reader.ReadToEnd();
			}
			return str;
		}

		public void WriteToFile(string path, string content)
		{
			using(StreamWriter stream = new StreamWriter(path))
			{
				stream.Write(content);
			}

		}

		public string ReadFromFile()
		{
			var fileContent = string.Empty; 
			using (var openFileDialog = new OpenFileDialog())
			{
				GetOptionalDialog(openFileDialog);

				if (openFileDialog.ShowDialog() == DialogResult.OK)
				{
					var filePath = openFileDialog.FileName;
					var fileStream = openFileDialog.OpenFile();

					using (StreamReader reader = new StreamReader(fileStream))
					{
						fileContent = reader.ReadToEnd();
					}
				}
			}
			return fileContent;
		}

		public string GetPathFile()
		{
			var openFileDialog = new OpenFileDialog();

			GetOptionalDialog(openFileDialog);

			openFileDialog.ShowDialog();

			return openFileDialog.FileName;
		}

		public void WriteRoFile(string data, string path)
		{
			using (StreamWriter writer = new StreamWriter(path))
			{
				writer.Write(data);
			}
		}

		public void GetOptionalDialog(System.Windows.Forms.FileDialog fileDialog)
		{
			fileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
		}

	}
}
