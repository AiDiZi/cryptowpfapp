﻿using Crypto.Algorithms;
using Crypto.Helpers;
using Crypto.Helpers.Representation;
using Crypto.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;

namespace Crypto
{
	/// <summary>
	/// Логика взаимодействия для MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{

		string outPath = string.Empty;

		string outPathKey = string.Empty;

		FileWorker _fileWorker = new FileWorker();

		Representation nowRepresentation = new Symbols();

		public MainWindow()
		{
			InitializeComponent();
			FillKeyPrompt();
		}

		public void ReadFromFile(object sender, EventArgs e)
		{
			var res = _fileWorker.ReadFromFile();
			if (!string.IsNullOrWhiteSpace(res))
			{
				var nowResRepreentation = nowRepresentation.GetRepresentation(res);
				inputArea.Text = nowResRepreentation;
			}
		}

		private void CheckInputFileKey(object sender, EventArgs e)
        {
			var res = _fileWorker.ReadFromFile();
			if (!string.IsNullOrWhiteSpace(res))
			{
				keyArea.Text = res;
			}
		}

		public void CheckOutFile(object sender, EventArgs e)
		{
			outPath = _fileWorker.GetPathFile();
			if (outPath != string.Empty)
				outPathLabel.Content = "Выходной файл для зашифрованного текста: " + outPath;
		}

		public void CheckOutFileKey(object sender, EventArgs e)
		{
			outPathKey = _fileWorker.GetPathFile();
			if (outPathKey != string.Empty)
				outPathKeyLabel.Content = "Выходной файл для ключа: " + outPathKey;
		}


		private char[] Realization(Func<char[], char[], char[], char[]> function, string inputStr, string keyStr)
		{
			var key = keyStr.ToCharArray();
			char[] input = inputStr.Trim().ToCharArray();
			var outChars = function.Invoke(input, key, CharsSets.GetUnicode());

			return outChars;
		}

		private string GetNameAlgorithm()
		{
			var nowSelect = AlgorithmSelect.SelectedItem as ComboBoxItem;
			return nowSelect.Name;
		}

		private void FillOutArea(string text)
		{
			outputArea.Text = text;

		}

		private void CryptHandler(object sender, EventArgs e)
		{
			var nameAlg = GetNameAlgorithm();
			var algorithm = AlgorithmDictionary.algorithmDictionary[nameAlg];
			var keyValidPredicate = AlgorithmValidatorKey.algorithmDictionary[nameAlg];

			if (keyValidPredicate(keyArea.Text, inputArea.Text))
			{
				Binary binary = new Binary();
				var nowInput = inputArea.Text;
				var nowKey = keyArea.Text;
				if(algorithm is SingleGamming)
                {
					if(!(nowRepresentation is Binary))
                    {
						var oldArrBytes = nowRepresentation.GetArrBytes(nowInput);
						var oldArrBytesKey = nowRepresentation.GetArrBytes(nowKey);
						nowInput = binary.GetRepresentation(oldArrBytes);
						nowKey = binary.GetRepresentation(oldArrBytesKey);
					}
                }
			
				var isValidInput = true;
                if (MiddleSquareCipher.IsSelected)
                {
					if (!IsValidDouble(inputArea.Text))
						isValidInput = false;
                }
                if (MiddleCompositionCipher.IsSelected)
                {
					if (!IsValidCoupleInt(inputArea.Text))
						isValidInput = false;
				}
				if (isValidInput)
				{
					var outText = Realization(algorithm.Crypt, nowInput, nowKey);
					if (algorithm is SingleGamming)
					{
						var outStr = string.Empty;
						for (int i = 0; i < outText.Length; i++)
							outStr += outText[i];
						var arrBytes = binary.GetArrBytes(outStr);
						outText = nowRepresentation.GetRepresentation(arrBytes).ToCharArray();
					}
					FillOut(outText);
				}
                else
                {
					if(MiddleSquareCipher.IsSelected)
						MessageBox.Show("Входная строка должна быть числом с \"плавающей точкой\", меньше 1 и 4 знаками после запятой");
					if(MiddleCompositionCipher.IsSelected)
						MessageBox.Show("Входная стрко имела неверный формат");
				}
			}
			else
				MessageBox.Show("Для данного шифра ключ имел неверный формат");

		}

		private bool IsValidDouble(string inputStr)
        {
			bool isValid = true;
			if (inputStr.Length == 6)
			{
				double dou;
				if(double.TryParse(inputStr,out dou))
                {
					if (!(dou < 1 && dou > 0))
						isValid = false;
                }
                else
                {
					isValid = false;
                }
			}
			else
				isValid = false;
			return isValid;
        }

		private bool IsValidCoupleInt(string inputStr)
        {
			bool isValid = true;
			var inputsStrArr = inputStr.Split(' ');
			if (inputsStrArr.Length == 2)
			{
				if (inputsStrArr[0].Length == 4 && inputsStrArr[1].Length == 4)
                {
					int first;
					int second;
					if (!(int.TryParse(inputsStrArr[0], out first) && int.TryParse(inputsStrArr[1], out second)))
						return false;
                }
                else
                {
					isValid = false;
                }
            }
            else
            {
				isValid = false;
            }

			return isValid;
        }

		public void SelectAlgorithm(object sender, EventArgs e)
		{
			FillKeyPrompt();
			if (inputArea != null)
			{
				inputArea.Text = string.Empty;
				keyArea.Text = string.Empty;
				outputArea.Text = string.Empty;
			}

		}

		private void DeleteButtonAnalysis()
        {
			if(buttonStackPanel.Children.Count  == 4)
				buttonStackPanel.Children.Remove(buttonStackPanel.Children[buttonStackPanel.Children.Count - 1]);
        }

		private void AddButtonAnalysis()
        {
			var buttonAnalysis = new Button();
			buttonAnalysis.Margin = new Thickness(50, 0, 0, 0);
			buttonAnalysis.Click += ShowAnalysis;
			buttonAnalysis.Content = "Показать период скремблера";
			buttonStackPanel.Children.Add(buttonAnalysis);
        }

		private void ShowAnalysis(object sender, EventArgs e)
        {
			var nowInput = inputArea.Text;
			var nowKey = keyArea.Text;
			var nameAlg = GetNameAlgorithm();
			var keyValidPredicate = AlgorithmValidatorKey.algorithmDictionary[nameAlg];
			if (keyValidPredicate(keyArea.Text, inputArea.Text))
			{
				if (!(nowRepresentation is Binary))
				{
					Binary binary = new Binary();
					var oldArrBytes = nowRepresentation.GetArrBytes(nowInput);
					nowInput = binary.GetRepresentation(oldArrBytes);
				}
				
			}
			else
				MessageBox.Show("Ключ имел неверный формат");
        }

		private void FillKeyPrompt()
        {
			var nameAlg = GetNameAlgorithm();
			var nowPromptKey = AlgorithmKeyPrompt.algorithmKeyDictionary[nameAlg];
			if (KeyPrompt != null)
				KeyPrompt.Text = nowPromptKey;
		}

		private void DecipherHandler(object sender, EventArgs e)
		{
			var nameAlg = GetNameAlgorithm();
			var algorithm = AlgorithmDictionary.algorithmDictionary[nameAlg];
			var keyValidPredicate = AlgorithmValidatorKey.algorithmDictionary[nameAlg];

			if (keyValidPredicate(keyArea.Text, inputArea.Text))
			{
				Binary binary = new Binary();
				var nowInput = inputArea.Text;
				var nowKey = keyArea.Text;
				if (algorithm is SingleGamming)
				{
					if (!(nowRepresentation is Binary))
					{
						var oldArrBytes = nowRepresentation.GetArrBytes(nowInput);
						var oldArrBytesKey = nowRepresentation.GetArrBytes(nowKey);
						nowInput = binary.GetRepresentation(oldArrBytes);
						nowKey = binary.GetRepresentation(oldArrBytesKey);
					}
				}

				var isValidInput = true;
				if (MiddleSquareCipher.IsSelected)
				{
					if (!IsValidDouble(inputArea.Text))
						isValidInput = false;
				}
				if (MiddleCompositionCipher.IsSelected)
				{
					if (!IsValidCoupleInt(inputArea.Text))
						isValidInput = false;
				}
				if (isValidInput)
				{
					var outText = Realization(algorithm.Decipher, nowInput, nowKey);
					if (algorithm is SingleGamming)
					{
						var outStr = string.Empty;
						for (int i = 0; i < outText.Length; i++)
							outStr += outText[i];
						var arrBytes = binary.GetArrBytes(outStr);
						outText = nowRepresentation.GetRepresentation(arrBytes).ToCharArray();
					}
					FillOut(outText);
				}
                else
                {
					if (MiddleSquareCipher.IsSelected)
						MessageBox.Show("Входная строка должна быть числом с \"плавающей точкой\", меньше 1 и 4 знаками после запятой");
					if (MiddleCompositionCipher.IsSelected)
						MessageBox.Show("Входная стрко имела неверный формат");
				}
			}
			else
				MessageBox.Show("Для данного шифра ключ имел неверный формат");


		}

		private void FillOut(char[] data)
		{
			var res = string.Empty;
			for (int i = 0; i < data.Length; i++)
				res += data[i];

			if (Convert.ToBoolean(WriteToUI.IsChecked))
			{
				FillOutArea(res);
			}
			else
			{
				if (!string.IsNullOrWhiteSpace(outPath))
				{
					_fileWorker.WriteToFile(outPath, res);
					MessageBox.Show($"Текст успешно записан в файл: {outPath}");
				}
				else
					MessageBox.Show("Неверно определен путь выходного файла");
			}
			if (!string.IsNullOrWhiteSpace(outPathKey))
			{
				_fileWorker.WriteToFile(outPathKey, keyArea.Text);
				MessageBox.Show($"Ключ успешно записан в файл: {outPathKey}");
			}
			else
				MessageBox.Show("Неверно определен путь выходного файла для ключа");
		}

		private void ViewCheckFileBut(object sender, EventArgs e)
		{
			CheckOutBut.Opacity = 1;
		}

		private void CloseCheckFileBut(object sender, EventArgs e)
		{
			CheckOutBut.Opacity = 0;
			outPath = string.Empty;
			outPathLabel.Content = string.Empty;
		}

		private void ChangeValuesAreas(object sender, EventArgs e)
		{
			if (outputArea.Text != string.Empty)
			{
				inputArea.Text = outputArea.Text;
				outputArea.Text = string.Empty;
			}
		}

		private int GetKeyLenght(int inputLenght)
        {
			var nowCoef = 1;
			if (nowRepresentation is Symbols)
				nowCoef = 16;
			if (nowRepresentation is Hex)
				nowCoef = 4;
			return inputLenght * nowCoef;

		}


		private void GenerateKey(object sender, EventArgs e)
        {
			var binary = KeyGenerator.GetRandomKey(GetKeyLenght(inputArea.Text.Length));
			var binartRep = new Binary();
			var arrBytes = binartRep.GetArrBytes(binary);
			if (nowRepresentation is Binary)
				keyArea.Text = binary;
			else
				keyArea.Text = nowRepresentation.GetRepresentation(arrBytes);
        }
		
		private void ChangeCodeSymbols(object sender, EventArgs e)
        {
            if (Binary.IsSelected)
            {
				if(SingleGammingCipher.IsSelected)
					TranformToNowRepresentationAll(new Binary());
		
			}
			else if (Hex.IsSelected)
            {
				if (SingleGammingCipher.IsSelected)
					TranformToNowRepresentationAll(new Hex());
			
			}
            else
            {
				if (SingleGammingCipher.IsSelected)
					TranformToNowRepresentationAll(new Symbols());
			
			}
        }

		private void TranformToNowRepresentation(Representation representation)
        {
            var oldArrBytesInputText = nowRepresentation.GetArrBytes(inputArea.Text);
            var oldArrBytesOut = nowRepresentation.GetArrBytes(outputArea.Text);
            nowRepresentation = representation;
            var newText = nowRepresentation.GetRepresentation(oldArrBytesInputText);
            var newOut = nowRepresentation.GetRepresentation(oldArrBytesOut);
            inputArea.Text = newText;
            outputArea.Text = newOut;
        }

		private void TranformToNowRepresentationAll(Representation representation)
        {
			if (inputArea != null)
			{
				var oldArrBytesInputText = nowRepresentation.GetArrBytes(inputArea.Text);
				var oldArrBytesOut = nowRepresentation.GetArrBytes(outputArea.Text);
				var oldArrBytesKey = nowRepresentation.GetArrBytes(keyArea.Text);
				nowRepresentation = representation;
				var newKey = nowRepresentation.GetRepresentation(oldArrBytesKey);
				var newText = nowRepresentation.GetRepresentation(oldArrBytesInputText);
				var newOut = nowRepresentation.GetRepresentation(oldArrBytesOut);
				inputArea.Text = newText;
				outputArea.Text = newOut;
				keyArea.Text = newKey;
			}
        }

		private bool IsValidSymbols(string symbols)
        {
			var isValid = true;
			if (nowRepresentation is Binary || nowRepresentation is Hex) {
				var nowRightChars = new List<char>();
				if (nowRepresentation is Binary)
				{
					nowRightChars.Add('0');
					nowRightChars.Add('1');
				}
				if (nowRepresentation is Hex)
				{
					for (var i = 0; i < 10; i++)
					{
						nowRightChars.Add(char.Parse(i.ToString()));
					}
					for (char ch = 'A'; ch <= 'F'; ch++)
					{
						nowRightChars.Add(ch);
					}
				}

				for (int i = 0; i < symbols.Length; i++)
                {
					var nowValid = false;
					for(int j=0; j< nowRightChars.Count; j++)
                    {
						if (symbols[i] == nowRightChars[j])
							nowValid = true;
                    }
					isValid = nowValid;
					if (!nowValid)
						break;
                }
			}
			return isValid;
				
			
			
        }

		

		private void ChangeInputHandler(object sender, TextChangedEventArgs e)
        {
			if (!IsValidSymbols(inputArea.Text))
            {
				var nowValue = inputArea.Text;
				nowValue = nowValue.Remove(nowValue.Length - 1);
				inputArea.Text = nowValue;
			}
        }

		private void ChangeOutHandler(object sender, TextChangedEventArgs e)
		{
			if (!IsValidSymbols(outputArea.Text))
			{
				var nowValue = outputArea.Text;
				nowValue = nowValue.Remove(nowValue.Length - 1);
				outputArea.Text = nowValue;
			}
		}

		private void ChangeKeyHandler(object sender, TextChangedEventArgs e)
		{
			if (!IsValidSymbols(keyArea.Text))
			{
				var nowValue = keyArea.Text;
				nowValue = nowValue.Remove(nowValue.Length - 1);
				keyArea.Text = nowValue;
			}
		}

	}
}
