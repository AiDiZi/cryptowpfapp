﻿using Crypto.Algorithms;
using Crypto.Helpers;
using Crypto.Models;
using Crypto.Statistics;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Crypto
{
	/// <summary>
	/// Логика взаимодействия для FrequencyAnalysisWindow.xaml
	/// </summary>
	public partial class FrequencyAnalysisWindow : Window
	{

		private readonly string _inputStr;

		private string _lastStr;

		private int _nowPairActiveIndex = -1;

		private CalcChars _calcChars;

		private List<Pair> pairs = new List<Pair>();

		private readonly CaesarCipher caesarCipher = new CaesarCipher();

		private CharCountPair[] _charCountPairs;

		private int _nowLang = 0;

		public FrequencyAnalysisWindow(string inputText)
		{
			InitializeComponent();

			if (!string.IsNullOrWhiteSpace(inputText))
			{
				_lastStr = inputText;
				_inputStr = inputText;
				inputTextArea.Text = inputText;
			}
			else
			{
				_lastStr = inputTextArea.Text;
				_inputStr = inputTextArea.Text;
			}
		

			_calcChars = new CalcChars();

			var pairs = _calcChars.GetStatistic(_inputStr, CharsSets.GetUnicode());
			var sortedPairs = (from pair in pairs orderby pair.Count descending select pair).ToArray();
			_charCountPairs = sortedPairs;


		}

		private void SelectLanguage(object sender, EventArgs e)
		{
			if (Russian.IsSelected)
				_nowLang = 0;
			else
				_nowLang = 1;
		}

		private void ShowOrigin(object sender, EventArgs e)
		{
			inputTextArea.Text = _lastStr;
		}

		private void CalcStatistic(object sender, EventArgs e)
		{
			var pairs = _calcChars.GetStatistic(inputTextArea.Text, CharsSets.GetUnicode());
			var sortedPairs = (from pair in pairs orderby pair.Count descending select pair).ToArray();
			_charCountPairs = sortedPairs;
		}

		private void Decipher(object sender, EventArgs e)
		{
			var originStatistics = new List<CharCountPair>(); 
			var symbols = new List<char>();
			if (_nowLang == 0)
			{
				for (var ch = 'а'; ch <= 'я'; ch++)
					symbols.Add(ch);
				originStatistics = OriginStaticstics.GetCharCountPairsRussian();
			}
			else
			{
				for (var ch = 'a'; ch <= 'z'; ch++)
					symbols.Add(ch);
				originStatistics = OriginStaticstics.GetCharCountPairsEnglish();
			}
			CircleArray circleArray = new CircleArray(symbols.ToArray());
			var orgIndex = circleArray.GetIndexBySymbols(originStatistics[0].Symbol);
			var nowIndex = circleArray.GetIndexBySymbols(_charCountPairs[0].Symbol);

			var nowInputText = string.Empty;

			foreach (var ch in inputTextArea.Text)
			{
				if (char.IsUpper(ch))
					nowInputText += char.ToLower(ch);
				else
					nowInputText += ch;
			}

			var nowText = caesarCipher.Decipher(nowInputText.ToCharArray(), (Math.Abs(nowIndex - orgIndex)).ToString().ToCharArray(), symbols.ToArray());

			inputTextArea.Text = string.Empty;
			for(int i=0; i< nowText.Length;i++)
			{
				inputTextArea.Text += nowText[i];
			}

			_lastStr = inputTextArea.Text;

		}


		private bool IsValidAddPair(string crypt, string changed)
		{
			if (string.IsNullOrWhiteSpace(crypt)) {
				MessageBox.Show("Заменяемая строка не может быть пустой или являться строкой, состоящей из символов разделителей");
				return false;
			}

			if (string.IsNullOrWhiteSpace(changed)){
				MessageBox.Show("Cтрока заменитель не может быть пустой или являться строкой, состоящей из символов разделителей");
				return false;
			}

			if(pairs.Any(item => item.OriginChar == crypt))
			{
				MessageBox.Show("Эта строка уже была заменена");
				return false;
			}

			return true;
		}

		private void AddPairs(object sender, EventArgs e)
		{
			if(IsValidAddPair(cryptChatArea.Text, changeCharArea.Text))
			{
				var origin = cryptChatArea.Text;
				var changed = changeCharArea.Text;
				var pair = new Pair(origin, changed);
				pairs.Add(pair);
				AddPairInUI(pair);
				cryptChatArea.Text = "";
				changeCharArea.Text = "";
			}
		}


		private void SelectionChangedPair(object sender, SelectionChangedEventArgs args)
		{
			var lbitems = args.AddedItems;
			if (lbitems.Count == 1)
			{
				_nowPairActiveIndex = pairsBlock.Items.IndexOf(lbitems[0]);
				cryptChatArea.Text = pairs[_nowPairActiveIndex].OriginChar;
				changeCharArea.Text = pairs[_nowPairActiveIndex].ChangedChar;
			}
		}

	

		private void AddPairInUI(Pair pair)
		{
			var stackPanel = GetPairUI(pair.OriginChar.ToString(), pair.ChangedChar.ToString());
			pairsBlock.Items.Add(stackPanel);
		}

		private UIElement GetPairUI(string letfValue, string rightValue)
		{
			var stackPanel = new StackPanel();
			stackPanel.Orientation = Orientation.Horizontal;

			var origin = new TextBlock();
			var changed = new TextBlock();
			origin.Text = letfValue + " - ";
			changed.Text = rightValue;
			stackPanel.Children.Add(origin);
			stackPanel.Children.Add(changed);
			return stackPanel;
		}

		private void DeletePair(object sender, EventArgs e)
		{
			if (_nowPairActiveIndex != -1)
			{
				var nowPair = pairs[_nowPairActiveIndex];
				pairs.Remove(nowPair);
				pairsBlock.Items.Remove(pairsBlock.Items[_nowPairActiveIndex]);
				_nowPairActiveIndex = -1;
				cryptChatArea.Text = string.Empty;
				changeCharArea.Text = string.Empty;

			}
				
		}


		private void ModifyText(object sender, EventArgs e)
		{
			StringBuilder stringBuilder = new StringBuilder(_lastStr);
			for (int i = 0; i < stringBuilder.Length; i++)
			{
				foreach (var pair in pairs)
				{
					if (pair.OriginChar == stringBuilder[i].ToString())
					{
						stringBuilder[i] = pair.ChangedChar.ToCharArray()[0];
						break;
					}
					if (pair.OriginChar.ToUpper() == stringBuilder[i].ToString())
					{
						stringBuilder[i] = pair.ChangedChar.ToUpper().ToCharArray()[0];
						break;
					}
					
				}
			}

			var nowStr = stringBuilder.ToString();
			inputTextArea.Text = nowStr;
			_lastStr = nowStr;
		} 

		private void ShowStatistic(object sender, EventArgs e)
		{
			
			StringBuilder stringBuilder = new StringBuilder(string.Empty);
			stringBuilder.Append("Вхождения символов из алфавита (топ 10 первых символов): \n");
			for (int i = 0; i < 10; i++)
			{
				stringBuilder.Append($"{_charCountPairs[i].Symbol} - {_charCountPairs[i].Count}, частота вхождения символа {_charCountPairs[i].Frequency.ToString().Substring(0,5)}\n");
			}
			MessageBox.Show(stringBuilder.ToString(), "Информация");
		}


		
		

		private void UpdatePair(object sender, EventArgs e)
		{
			if (_nowPairActiveIndex != -1 && !string.IsNullOrWhiteSpace(cryptChatArea.Text) && !string.IsNullOrWhiteSpace(changeCharArea.Text))
			{
				pairs[_nowPairActiveIndex].ChangedChar = changeCharArea.Text;
				pairs[_nowPairActiveIndex].OriginChar = cryptChatArea.Text;

				pairsBlock.Items[_nowPairActiveIndex] = GetPairUI(cryptChatArea.Text, changeCharArea.Text);
				
			}
		}




	}
}
