﻿namespace Crypto
{
	public class CircleArray 
	{

		private char[] symbols;
		
		public CircleArray(char[] symbols)
		{
			this.symbols = symbols;
		}

		public char GetSymbolsByIndex(int index)
		{
			var coef = index / symbols.Length;

			if (coef < 1)
				coef = 1;
			
			var nowIndex = index;
			if (index >= symbols.Length )
				nowIndex = index / coef  - symbols.Length;
			if (index < 0)
				nowIndex = symbols.Length + index / coef;
			return symbols[nowIndex];
		}

		
		public int GetIndexBySymbols(char symbol) 
		{
			var isChange = false;
			var index = 0;
			for(int i = 0; i < symbols.Length; i++)
			{
				if (symbol == symbols[i])
				{
					index = i;
					isChange = true;
				}
				
			}
			if (isChange)
				return index;
			else
				return -1;
		}


	}
}
