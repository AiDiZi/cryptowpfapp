﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crypto.Models
{
    public class OldNewIndex
    {
        public int OldIndex { get; set; }

        public int NewIndex { get; set; }

        public OldNewIndex(int old, int newIndex)
        {
            OldIndex = old;
            NewIndex = newIndex;
        }

    }
}
