﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crypto.Models
{
	public class Pair
	{

		public string OriginChar { get; set; }

		public string ChangedChar { get; set; }

		public Pair(string originChar, string changedChar)
		{
			OriginChar = originChar;
			ChangedChar = changedChar;
		}

	}
}
