﻿using System;
using System.Collections.Generic;

namespace Crypto
{
	public class CharsSets
	{

		public static char[] GetUnicode()
		{
			List<char> alphabet = new List<char>();

			for(var ch = 'a'; ch <= 'z' ; ch++)
			{
				alphabet.Add(ch);
			}
			for (var ch = 'A'; ch <= 'Z'; ch++)
			{
				alphabet.Add(ch);
			}
			for (var ch = 'а'; ch <= 'я'; ch++)
			{
				alphabet.Add(ch);
			}

			for (var ch = 'А'; ch <= 'Я'; ch++)
			{
				alphabet.Add(ch);
			}

			for (var ch = '0'; ch <= '9'; ch++)
			{
				alphabet.Add(ch);
			}


			var chars = alphabet.ToArray();

			return chars;
		}
	}
}
