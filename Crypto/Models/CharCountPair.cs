﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crypto.Models
{
	public class CharCountPair
	{

		public char Symbol { get; set; }

		public int Count { get; set; }

		public double Frequency { get; set; }

		public CharCountPair(char symbol, int count)
		{
			Symbol = symbol;
			Count = count;
		}

		public CharCountPair(char symbol, int count, double frequency)
		{
			Symbol = symbol;
			Count = count;
			Frequency = frequency;
		}

	}
}
