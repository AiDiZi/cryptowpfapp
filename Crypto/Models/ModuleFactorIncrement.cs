﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crypto.Models
{
    public class ModuleFactorIncrement
    {
        public double Module { get; set; }

        public double Factor { get; set; }

        public double Increment { get; set; }

        public ModuleFactorIncrement(double module, double factor, double increment)
        {
            Module = module;
            Factor = factor;
            Increment = increment;
        }

    }
}
