﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Crypto.Algorithms;

namespace Crypto.Models
{
	public class AlgorithmDictionary
	{
		public static Dictionary<string, IAlgorithm> algorithmDictionary = new Dictionary<string, IAlgorithm>
		{
			["Caesar"] = new CaesarCipher(),
			["Viginer"] = new ViginerAlgoritm(),
			["TabularRoutingPermutation"] = new TabularRoutingPermutationCipher(),
			["Wandering"] = new WanderingAlgorithm(),
			["VerticalPermutationCipher"] = new VerticalPermutationCipher(),
			["DoublePermutationCipher"] = new DoublePermutationCipher(),
			["SwivelGrille"] = new SwivelGrille(),
			["SingleGammingCipher"] = new SingleGamming(),
			["MiddleSquareCipher"] = new MiddleSquares(),
			["MiddleCompositionCipher"] = new MiddleComposition(),
			["LinearCongruentCipher"] = new LinearCongruentCipher(),
		};
	}


}
