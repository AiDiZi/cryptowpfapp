﻿using Crypto.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crypto.Models
{
	public class AlgorithmValidatorKey
	{
		public static Dictionary<string, Func<string, string, bool>> algorithmDictionary = new Dictionary<string, Func<string, string, bool>>
		{
			["Caesar"] = KeyValidator.IsValidKeyNumber,
			["Viginer"] = KeyValidator.IsValidKeyNumbersChars,
			["TabularRoutingPermutation"] = KeyValidator.IsValidKeyNumber,
			["Wandering"] = KeyValidator.IsValidKeyNumber,
			["VerticalPermutationCipher"] = KeyValidator.IsValidKeyNumbersChars,
			["DoublePermutationCipher"] = KeyValidator.IsValidTwoKeys,
			["SwivelGrille"] = KeyValidator.IsValidGrilleKey,
			["SingleGammingCipher"] = KeyValidator.IsKeyEqStr,
			["ScramblerCipher"] = KeyValidator.IsValidKeyScrem,
			["MiddleSquareCipher"] = KeyValidator.IsValidKeyNumber,
			["MiddleCompositionCipher"] = KeyValidator.IsValidKeyNumber,
			["LinearCongruentCipher"] = KeyValidator.IsValidModuleFactorInc,
		};
	}
}
