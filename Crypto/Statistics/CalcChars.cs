﻿using Crypto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crypto.Statistics
{
	public class CalcChars
	{

		public List<CharCountPair> GetStatistic(string text, char[] alphabet)
		{
			var pairs = new List<CharCountPair>();
			var nowText = text;
			while(nowText != string.Empty)
			{
				var nowCh = nowText[0];
				var count = CalcCountChar(nowText, nowCh);
				if (char.IsUpper(nowCh))
				{
					nowText = nowText.Replace(nowCh.ToString(), "").Replace(char.ToLower(nowCh).ToString(), "");
					nowCh = char.ToLower(nowCh);
				}
				else
				{
					nowText = nowText.Replace(nowCh.ToString(), "").Replace(char.ToUpper(nowCh).ToString(), "");
				}
				if(alphabet.Any(item => item == nowCh))
					pairs.Add(new CharCountPair(nowCh, count));
			}

			var sum = 0;

			foreach(var pair in pairs)
			{
				sum += pair.Count;
			}

			foreach(var pair in pairs)
			{
				pair.Frequency = Convert.ToDouble(pair.Count) / Convert.ToDouble(sum);
			}

			return pairs;
		}

		private int CalcCountChar(string text, char ch)
		{
			var count = 0;
			foreach(var nowCh in text)
			{
				if (nowCh == ch || char.ToUpper(ch) == nowCh)
					count++;
			}

			return count;
		}
	}
}
